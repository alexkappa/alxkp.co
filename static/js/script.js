$('#shorten').click(function() {
	var url = $('#url').val();
	$.ajax({
		url: "/",
		method: "POST",
		data: {
			url: url
		}
	}).done(function(shortened) {
		$("#result").text(shortened)
	});
	return false
});

$("#result").click(function() {
	var doc = document,
		text = this,
		range,
		selection;
	if (doc.body.createTextRange) { //ms
		range = doc.body.createTextRange();
		range.moveToElementText(text);
		range.select();
	} else if (window.getSelection) { //all others
		selection = window.getSelection();
		range = doc.createRange();
		range.selectNodeContents(text);
		selection.removeAllRanges();
		selection.addRange(range);
	}
});

WebFontConfig = {
	fontdeck: {
		id: '14718'
	}
};
(function() {
	var wf = document.createElement('script');
	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
})();