package main

import (
	"flag"
	"html/template"
	"log"
	"net/http"
	"net/url"

	"github.com/codegangsta/martini"
	"github.com/dchest/uniuri"
	"github.com/hoisie/redis"
)

var (
	host = flag.String("host", "alxkp.localhost:8080", "Address to listen for connections")
	klen = flag.Int("k", 3, "Key length in shortened URLs")
)

func init() {
	flag.Parse()
}

func main() {
	r := martini.NewRouter()
	m := martini.New()
	m.Map(redis.Client{})
	m.Use(martini.Logger())
	m.Use(martini.Recovery())
	m.Use(martini.Static("static"))
	m.Action(r.Handle)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		t := template.Must(template.ParseFiles("templates/index.html"))
		err := t.Execute(w, nil)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
	})

	r.Get("/:name", func(w http.ResponseWriter, params martini.Params, rds redis.Client, log *log.Logger) {
		value, err := rds.Get(params["name"])
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("The specified URL was not found."))
			if martini.Env == martini.Dev {
				w.Write([]byte(err.Error()))
			}
			return
		}
		w.Header().Add("Location", string(value))
		w.WriteHeader(http.StatusMovedPermanently)
	})

	r.Post("/", func(w http.ResponseWriter, r *http.Request, rds redis.Client) {
		err := r.ParseForm()
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Failed to parse request parameters"))
			return
		}
		value := r.Form.Get("url")
		if value == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("URL parameter not found"))
			return
		}
		url, err := url.Parse(value)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Invalid URL"))
			return
		}
		key := uniuri.NewLen(*klen)
		rds.Set(key, []byte(url.String()))
		shortened, _ := url.Parse("http://" + *host + "/" + key)
		w.Header().Add("Location", value)
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(shortened.String()))
	})

	http.ListenAndServe(*host, m)
}
